package br.edu.utfpr.cashback.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.edu.utfpr.cashback.entity.Usuario;
import br.edu.utfpr.cashback.repository.UsuarioRepository;
import br.edu.utfpr.cashback.security.UserAuthDetails;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{ 

	@Autowired private UsuarioRepository repo;
	
	@Override
	public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
		Usuario u = repo.findByLogin( username ).orElseThrow( () -> new UsernameNotFoundException( "Usuário inválido!" ) );		
		return UserAuthDetails.build( u );
	}
	
}