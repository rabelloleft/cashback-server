package br.edu.utfpr.cashback.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.utfpr.cashback.entity.CupomFiscal;

@Repository
public interface CupomFiscalRepository extends JpaRepository<CupomFiscal, Integer> {

	List<CupomFiscal> findByEmpresa_Id( Integer empresa );
	List<CupomFiscal> findByUsuario_Id( Integer usuario );
	boolean deleteByEmpresa_Id( Integer id );
	boolean deleteByUsuario_Id( Integer id );
	
}
