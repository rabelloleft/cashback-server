package br.edu.utfpr.cashback.entity.dto;

import java.util.List;

public class LoginReturn {

	private Integer id;	
	private String nome;	
	private String token;
	private List<String> papeis;
	
	public LoginReturn() { }
	public LoginReturn(Integer id, String nome, String token, List<String> papeis) {
		super();
		this.id = id;
		this.nome = nome;
		this.token = token;
		this.papeis = papeis;
	}

	public Integer getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }
	public String getToken() { return token; }
	public void setToken(String token) { this.token = token; }
	public List<String> getPapeis() { return papeis; }
	public void setPapeis(List<String> papeis) { this.papeis = papeis; }
		
}