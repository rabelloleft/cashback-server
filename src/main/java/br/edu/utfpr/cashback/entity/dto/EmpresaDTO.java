package br.edu.utfpr.cashback.entity.dto;

import br.edu.utfpr.cashback.entity.Empresa;

public class EmpresaDTO {
	
	private Integer id;
	private String nome;
	private String cnpj;
	private Integer idcategoria;
	private Integer idusuario;
	
	public EmpresaDTO() { }
	public EmpresaDTO( Empresa e ) {
		this.id = e.getId();
		this.nome = e.getNome();
		this.cnpj = e.getCnpj();
		this.idcategoria = e.getCategoria().getId();
		this.idusuario = e.getUsuario().getId();
	}
	
	public Integer getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }
	public String getCnpj() { return cnpj; }
	public void setCnpj(String cnpj) { this.cnpj = cnpj; }
	public Integer getIdcategoria() { return idcategoria; }
	public void setIdcategoria(Integer idcategoria) { this.idcategoria = idcategoria; }
	public Integer getIdusuario() { return idusuario; }
	public void setIdusuario(Integer idusuario) { this.idusuario = idusuario; }
		
}