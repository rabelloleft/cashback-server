package br.edu.utfpr.cashback.entity.dto;

import br.edu.utfpr.cashback.entity.Categoria;

public class CategoriaDTO {
	
	private Integer id;	
	private String descricao;	
	private Double percentual;
	
	public CategoriaDTO() { }
	public CategoriaDTO( Categoria c ) {
		this.id = c.getId();
		this.descricao = c.getDescricao();
		this.percentual = c.getPercentualCashback();
	}
	
	public Integer getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	public String getDescricao() { return descricao; }
	public void setDescricao(String descricao) { this.descricao = descricao; }
	public Double getPercentual() { return percentual; }
	public void setPercentual(Double percentual) { this.percentual = percentual; } 
	
}