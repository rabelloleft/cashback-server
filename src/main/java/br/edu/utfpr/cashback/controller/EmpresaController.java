package br.edu.utfpr.cashback.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.utfpr.cashback.entity.Empresa;
import br.edu.utfpr.cashback.entity.dto.EmpresaDTO;
import br.edu.utfpr.cashback.repository.CategoriaRepository;
import br.edu.utfpr.cashback.repository.CupomFiscalRepository;
import br.edu.utfpr.cashback.repository.EmpresaRepository;
import br.edu.utfpr.cashback.repository.UsuarioRepository;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired private CategoriaRepository categoria;
	@Autowired private CupomFiscalRepository cupom;
	@Autowired private EmpresaRepository repo;
	@Autowired private UsuarioRepository usuario;
	 
	
	@GetMapping
	@PreAuthorize("hasRole('ADMIN')")
	public List<EmpresaDTO> listar() {
		List<Empresa> empresas = repo.findAll();
		List<EmpresaDTO> ret = empresas.stream()
				.map( e -> new EmpresaDTO( e ) )
				.collect( Collectors.toList() );
		return ret;
	}
	
//	@GetMapping
//	@RequestMapping("/{id}")
//	@PreAuthorize("hasRole('ADMIN')")
//	public List<EmpresaDTO> listarById( @PathVariable("id") Integer id ) {
//		List<Empresa> empresas = repo.findByUsuario_Id( id );
//		List<EmpresaDTO> ret = empresas.stream()
//				.map( e -> new EmpresaDTO( e ) )
//				.collect( Collectors.toList() );
//		return ret;
//	}
	
	@GetMapping
	@RequestMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public EmpresaDTO getEmpresa( @PathVariable("id") Integer id ) {
		Empresa e = repo.findById( id ).orElse( null );
		return new EmpresaDTO( e );
	}
	
	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> save( @RequestBody EmpresaDTO dto ) {
		Empresa e = new Empresa();
		e.setNome( dto.getNome() );
		e.setCnpj( dto.getCnpj() );
		e.setCategoria( categoria.findById( dto.getIdcategoria() ).orElse( null ) );
		e.setUsuario( usuario.findById( dto.getIdusuario() ).orElse( null ) );
		repo.save( e );
		return ResponseEntity.ok().build();
	}	
	
	@DeleteMapping
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> delete( @RequestBody EmpresaDTO dto ) {	
		if ( cupom.deleteByEmpresa_Id( dto.getId() ) ) {
			repo.deleteById( dto.getId() );
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
	}

}