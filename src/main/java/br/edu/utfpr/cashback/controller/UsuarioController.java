package br.edu.utfpr.cashback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.utfpr.cashback.entity.Usuario;
import br.edu.utfpr.cashback.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired private UsuarioRepository repo;
	
	@GetMapping
	public ResponseEntity<?> listar() {
		List<Usuario> users = repo.findAll();
		return ResponseEntity.ok(users);
	}
	
}