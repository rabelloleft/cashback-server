package br.edu.utfpr.cashback.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.utfpr.cashback.entity.dto.CategoriaDTO;
import br.edu.utfpr.cashback.repository.CategoriaRepository;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
	
	@Autowired private CategoriaRepository repo;
	
	@GetMapping
	@PreAuthorize("hasRole('ADMIN')")
	public List<CategoriaDTO> listar() {
		return repo.findAllByOrderByDescricaoAsc().stream().map( c -> new CategoriaDTO( c ) ).collect( Collectors.toList() );
	}
	
	
}