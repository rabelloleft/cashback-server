package br.edu.utfpr.cashback.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.utfpr.cashback.entity.dto.LoginEntry;
import br.edu.utfpr.cashback.entity.dto.LoginReturn;
import br.edu.utfpr.cashback.security.JwtProvider;
import br.edu.utfpr.cashback.security.UserAuthDetails;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired private JwtProvider provider;	
	@Autowired private AuthenticationManager manager;
	
	@PostMapping
	public ResponseEntity<LoginReturn> login( @RequestBody LoginEntry loginEntry ) {				
		Authentication auth = manager.authenticate( new UsernamePasswordAuthenticationToken( loginEntry.getLogin(), loginEntry.getSenha() ) );

		SecurityContextHolder.getContext().setAuthentication(auth);
		String token = provider.generateToken(auth);
		
		UserAuthDetails details = (UserAuthDetails) auth.getPrincipal();

		LoginReturn ret = new LoginReturn();
		ret.setNome( details.getNome() );
		ret.setId( details.getId() );
		ret.setToken( token );
		
		List<String> papeis = details.getAuthorities()
				.stream().map( g -> g.getAuthority() ).collect( Collectors.toList() );
		ret.setPapeis( papeis );
		
		return ResponseEntity.ok( ret );
	}

}