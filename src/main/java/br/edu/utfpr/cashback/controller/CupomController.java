package br.edu.utfpr.cashback.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.utfpr.cashback.repository.CupomFiscalRepository;

@RestController
@RequestMapping("/cupom")
public class CupomController {

	@Autowired
	private CupomFiscalRepository repository;
	
	@GetMapping("/{empresa}")
	public ResponseEntity<?> listByEmpresa(@PathVariable("empresa") Integer empresa) {
		return ResponseEntity.ok(repository.findByEmpresa_Id(empresa));
	}
	
}
