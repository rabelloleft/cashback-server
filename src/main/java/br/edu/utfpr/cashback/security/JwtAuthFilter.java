package br.edu.utfpr.cashback.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import br.edu.utfpr.cashback.services.UserDetailsServiceImpl;

public class JwtAuthFilter extends OncePerRequestFilter {
	
	@Autowired private JwtProvider provider;	
	@Autowired private UserDetailsServiceImpl service;

	@Override
	protected void doFilterInternal( HttpServletRequest req, HttpServletResponse res, FilterChain chain ) throws ServletException, IOException {

		String token = getToken( req );
		
		if ( (token != null) && (provider.validateToken(token) ) ) {
			String login = provider.getUserFromToken( token );			
			UserDetails details = service.loadUserByUsername( login );
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken( details, null, details.getAuthorities() );
			auth.setDetails( new WebAuthenticationDetails( req ) );
			SecurityContextHolder.getContext().setAuthentication( auth );
		}
		
		chain.doFilter( req, res );
		
	}
	
	private String getToken( HttpServletRequest req ) {		
		String header = req.getHeader("Authorization");		
		if ( (header != null) && (header.startsWith("Bearer ")) ) {
			return header.replace("Bearer ", "");
		}		
		return null;
	}
	
}