package br.edu.utfpr.cashback.security;

import java.util.Date;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtProvider {
	
	private static final String KEY = "c4$Hb4cK";
	
	public String generateToken(Authentication auth) {
		
		UserAuthDetails details = (UserAuthDetails) auth.getPrincipal();

		Date now = new Date();
		Date exp = new Date( now.getTime() + (60 * 30 * 1000) );
		
		return Jwts.builder()
				.setSubject( details.getUsername() )
				.setIssuedAt( now )
				.setExpiration( exp )
				.signWith( SignatureAlgorithm.HS512, KEY )
				.compact();
	}
	
	public String getUserFromToken( String token ) {
		return Jwts.parser() 
				.setSigningKey( KEY ) 
				.parseClaimsJws( token ) 
				.getBody().getSubject(); 
	}
	
	public boolean validateToken(String token) {		
		try {
			Jwts.parser()
				.setSigningKey( KEY )
				.parseClaimsJws( token );
		} catch( Exception ex ) {
			System.out.println( "Invalid token " + "\nMessage: " + ex.getMessage() );
			return false;
		}		
		return true;
	}

}
